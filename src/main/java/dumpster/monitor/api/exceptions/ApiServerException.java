package dumpster.monitor.api.exceptions;

import java.net.HttpURLConnection;

/**
 * Extension of a {@link ApiException} for all internal server errors.
 */
public final class ApiServerException extends ApiException {

    private static final long serialVersionUID = -8179740066915316651L;

    public ApiServerException(final String message, final Throwable cause) {
        super(HttpURLConnection.HTTP_INTERNAL_ERROR, message, cause);
    }

    public ApiServerException(final Throwable cause) {
        super(HttpURLConnection.HTTP_INTERNAL_ERROR, cause);
    }

    public ApiServerException(final String message) {
        super(HttpURLConnection.HTTP_INTERNAL_ERROR, message);
    }
}
