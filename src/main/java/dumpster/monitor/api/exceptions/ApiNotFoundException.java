package dumpster.monitor.api.exceptions;

import java.net.HttpURLConnection;

/**
 * Extension of a {@link ApiException} for all not found dumpster.monitor.api.exceptions.
 */
public final class ApiNotFoundException extends ApiException {

    private static final long serialVersionUID = -797138628982938535L;

    /**
     * Constructor.
     *
     * @param message human readable message.
     * @param cause   reason for this exception.
     */
    public ApiNotFoundException(final String message, final Throwable cause) {
        super(HttpURLConnection.HTTP_NOT_FOUND, message, cause);
    }

    /**
     * Constructor.
     *
     * @param cause reason for this exception.
     */
    public ApiNotFoundException(final Throwable cause) {
        super(HttpURLConnection.HTTP_NOT_FOUND, cause);
    }

    /**
     * Constructor.
     *
     * @param message human readable message.
     */
    public ApiNotFoundException(final String message) {
        super(HttpURLConnection.HTTP_NOT_FOUND, message);
    }
}
