package dumpster.monitor.api.exceptions;

import java.net.HttpURLConnection;

public final class ApiPreconditionException extends ApiException {
    private static final long serialVersionUID = 3789654477792751647L;

    public ApiPreconditionException(final String message, final Throwable cause) {
        super(HttpURLConnection.HTTP_PRECON_FAILED, message, cause);
    }

    public ApiPreconditionException(final Throwable cause) {
        super(HttpURLConnection.HTTP_PRECON_FAILED, cause);
    }

    public ApiPreconditionException(final String message) {
        super(HttpURLConnection.HTTP_PRECON_FAILED, message);
    }
}
