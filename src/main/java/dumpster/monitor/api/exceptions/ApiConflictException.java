package dumpster.monitor.api.exceptions;

import java.net.HttpURLConnection;

public final class ApiConflictException extends ApiException {
    private static final long serialVersionUID = -4063019752726177172L;

    public ApiConflictException(final String message, final Throwable cause) {
        super(HttpURLConnection.HTTP_CONFLICT, message, cause);
    }

    public ApiConflictException(final Throwable cause) {
        super(HttpURLConnection.HTTP_CONFLICT, cause);
    }

    public ApiConflictException(final String message) {
        super(HttpURLConnection.HTTP_CONFLICT, message);
    }
}
