package dumpster.monitor.api.exceptions;

import java.net.HttpURLConnection;

public final class ApiBadRequestException extends ApiException {

    private static final long serialVersionUID = -7171949704612190585L;

    public ApiBadRequestException(final String message, final Throwable cause) {
        super(HttpURLConnection.HTTP_BAD_REQUEST, message, cause);
    }

    public ApiBadRequestException(final Throwable cause) {
        super(HttpURLConnection.HTTP_BAD_REQUEST, cause);
    }

    public ApiBadRequestException(final String message) {
        super(HttpURLConnection.HTTP_BAD_REQUEST, message);
    }
}
