package dumpster.monitor.api.services.impl;

import dumpster.monitor.api.events.CreateDumpsterEvent;
import dumpster.monitor.api.events.DeleteDumpsterEvent;
import dumpster.monitor.api.events.UpdateDumpsterEvent;
import dumpster.monitor.api.exceptions.ApiException;
import dumpster.monitor.api.exceptions.ApiNotFoundException;
import dumpster.monitor.api.model.Dumpster;
import dumpster.monitor.api.repository.jpa.DumpsterRepository;
import dumpster.monitor.api.services.DumpsterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Игорь Золотинцкий
 */
@Service
@Transactional(rollbackFor = {ApiException.class, ConstraintViolationException.class})
public final class DumpsterServiceImpl implements DumpsterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DumpsterServiceImpl.class);
    private final DumpsterRepository dumpsterRepository;
    private final ApplicationContext applicationContext;

    @Inject
    public DumpsterServiceImpl(final DumpsterRepository dumpsterRepository, final ApplicationContext applicationContext) {
        this.dumpsterRepository = dumpsterRepository;
        this.applicationContext = applicationContext;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Dumpster addDumpster (
            @NotNull
            @Valid
            final Dumpster dumpster
    ) throws ApiException {
        // Если такого нет, то на выход
        if (dumpsterRepository.findByPartNumber(dumpster.getPartNumber()).isPresent()) {
            throw new ApiNotFoundException("Устройство № " + dumpster.getPartNumber() + " уже существует");
        }

        final Dumpster newDumpster = dumpsterRepository.save(dumpster);
        applicationContext.publishEvent(new CreateDumpsterEvent(newDumpster));
        return newDumpster;
    }

    @Override
    public Dumpster removeDumpster(int partNumber) {
        // Если такого нет, то на выход
        dumpsterRepository.findByPartNumber(partNumber)
                .orElseThrow(() -> new ApiNotFoundException("Не зарегистрировано устройство № " + partNumber));

        dumpsterRepository.deleteByPartNumber(partNumber);
        applicationContext.publishEvent(new DeleteDumpsterEvent(partNumber));
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Dumpster updateDumpster (
            @NotNull
            @Valid
            final Dumpster dumpster
    ) throws ApiException {
        // Если такого нет, то на выход
        dumpsterRepository.findByPartNumber(dumpster.getPartNumber())
                .orElseThrow(() -> new ApiNotFoundException("Не зарегистрировано устройство № " + dumpster.getPartNumber()));

        final Dumpster updatedDumpster = dumpsterRepository.save(dumpster);
        applicationContext.publishEvent(new UpdateDumpsterEvent(updatedDumpster));
        return updatedDumpster;
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true)
    @Override
    public List<Dumpster> getDumpsters() throws ApiException {
        return dumpsterRepository.findAll();
    }
}
