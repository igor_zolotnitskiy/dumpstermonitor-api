package dumpster.monitor.api.services;

import dumpster.monitor.api.exceptions.ApiException;
import dumpster.monitor.api.model.Options;

import java.util.List;

/**
 * @author Игорь Золотинцкий
 */
public interface OptionsService {
    /**
     * Загрузить настройки
     *
     * @param id настроек
     */
    Options getOptions(final String id) throws ApiException;

    /**
     * Обновить настройки
     *
     * @param options настройки для обновления
     * @return обновленные настройки
     */
    Options updateOptions(Options options) throws ApiException;
}
