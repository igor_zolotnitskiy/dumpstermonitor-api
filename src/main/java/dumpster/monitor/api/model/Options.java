package dumpster.monitor.api.model;

import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * @author Игорь Золотинцкий
 */
@Entity
public class Options {
    public static String MAIN_OPTIONS = "main";

    @Id
    @Column(updatable = false, name = "id", unique = true)
    @NotNull
    private String id;

    @Column(name = "critical_status", nullable = false)
    @Range(min = 0, max = 100)
    private int criticalStatus;

    @Column(name = "critical_battery", nullable = false)
    @Range(min = 0, max = 100)
    private int criticalBattery;

    @Column(name = "critical_timeout", nullable = false)
    @Range(min = 0)
    private int criticalTimeout;

    protected Options() {
    }

    public String getId() {
        return id;
    }

    public int getCriticalStatus() {
        return criticalStatus;
    }

    public int getCriticalBattery() {
        return criticalBattery;
    }

    public int getCriticalTimeout() {
        return criticalTimeout;
    }

    @Override
    public String toString() {
        return String.format(
                "Options[id=%s, criticalStatus=%s, criticalBattery=%s, criticalTimeout=%s]",
                id, criticalStatus, criticalBattery, criticalTimeout);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Options options = (Options) o;
        return Objects.equals(id, options.id) &&
                Objects.equals(criticalStatus, options.criticalStatus) &&
                Objects.equals(criticalBattery, options.criticalBattery) &&
                Objects.equals(criticalTimeout, options.criticalTimeout);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                id,
                criticalStatus,
                criticalBattery,
                criticalTimeout);
    }
}
