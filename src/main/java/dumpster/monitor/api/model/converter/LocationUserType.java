package dumpster.monitor.api.model.converter;

import dumpster.monitor.api.model.Location;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.geometric.PGpoint;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

public class LocationUserType implements UserType {

  @Override
  public int[] sqlTypes() {
    return new int[]{
        Types.OTHER
    };
  }

  @Override
  public Class returnedClass() {
    return Location.class;
  }

  @Override
  public boolean equals(final Object x, final Object y) throws HibernateException {
    return Objects.equals(x, y);
  }

  @Override
  public int hashCode(final Object x) throws HibernateException {
    return Objects.hash(x);
  }

  @Override
  public Object nullSafeGet(
      final ResultSet rs,
      final String[] names,
      final SessionImplementor session,
      final Object owner
  ) throws HibernateException, SQLException {
    assert names.length == 1;

    PGpoint point = (PGpoint) rs.getObject(names[0]);
    return point == null ? null : new Location(point.x, point.y);
  }

  @Override
  public void nullSafeSet(
      final PreparedStatement st,
      final Object value,
      final int index,
      final SessionImplementor session
  ) throws HibernateException, SQLException {
    if (value == null) {
      st.setNull(index, Types.OTHER);
    } else {
      final Location point = (Location) value;
      st.setObject(index, new PGpoint(point.getLatitude(), point.getLongitude()));
    }
  }

  @Override
  public Object deepCopy(final Object value) throws HibernateException {
    return value;
  }

  @Override
  public boolean isMutable() {
    return false;
  }

  @Override
  public Serializable disassemble(final Object value) throws HibernateException {
    return (Serializable) value;
  }

  @Override
  public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
    return cached;
  }

  @Override
  public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
    return original;
  }
}
