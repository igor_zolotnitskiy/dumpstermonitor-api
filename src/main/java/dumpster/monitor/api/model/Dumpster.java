package dumpster.monitor.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
public class Dumpster {
    @Id
    @Column(updatable = false, name = "part_number", unique = true)
    @NotNull
    private int partNumber;

    @Column(nullable = true)
    @Type(type = "dumpster.monitor.api.model.converter.LocationUserType")
    private Location location;

    @Column(name = "status", nullable = true)
    @Range(min = 0, max = 100)
    private Short status;

    @Column(name = "battery_level", nullable = true)
    @Range(min = 0, max = 100)
    private Short batteryLevel;

    @Column(name = "updated_datetime", columnDefinition = "timestamp with time zone")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDatetime;

    protected Dumpster() {
    }

    public Dumpster(String message) {
        Map<String, String> map = new HashMap<String, String>();
        String[] params = message.split(";");

        Arrays.stream(params).forEach((s) -> {
            String[] d = s.split("\\s");
            map.put(d[0], d[1]);
        });

        partNumber = Integer.parseInt(map.get("part_number"));
        location = new Location(Float.parseFloat(map.get("latitude")), Float.parseFloat(map.get("longitude")));
        status = Short.parseShort(map.get("status"));
        batteryLevel = Short.parseShort(map.get("battery"));
        updatedDatetime = new Date();
    }

    public int getPartNumber() {
        return partNumber;
    }

    public Location getLocation() {
        return location;
    }

    public Short getStatus() {
        return status;
    }

    public Short getBatteryLevel() {
        return batteryLevel;
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public Date getUpdatedDatetime() {
        return updatedDatetime;
    }

    @Override
    public String toString() {
        return String.format(
                "Dumpster[partNumber=%s, location=%s, status=%s, batteryLevel=%s, updatedDatetime=%s]",
                partNumber, location, status, batteryLevel, updatedDatetime);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Dumpster dumpster = (Dumpster) o;
        return Objects.equals(partNumber, dumpster.partNumber) &&
                Objects.equals(location, dumpster.location) &&
                Objects.equals(status, dumpster.status) &&
                Objects.equals(batteryLevel, dumpster.batteryLevel) &&
                Objects.equals(updatedDatetime, dumpster.updatedDatetime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                partNumber,
                location,
                status,
                batteryLevel,
                updatedDatetime);
    }
}
