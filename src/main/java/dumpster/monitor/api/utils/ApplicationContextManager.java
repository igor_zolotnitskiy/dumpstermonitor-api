package dumpster.monitor.api.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author Игорь Золотницкий
 */
@Component
public class ApplicationContextManager implements ApplicationContextAware {
    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext ctx){
        context = ctx;
    }

    public static ApplicationContext getAppContext(){
        return context;
    }
}
