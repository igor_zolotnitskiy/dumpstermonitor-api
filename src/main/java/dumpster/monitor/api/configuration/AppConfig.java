package dumpster.monitor.api.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import udp.server.UDPServer;

import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * @author Igor Zolotnitskiy
 */
@Configuration
public class AppConfig {
  static private final Logger LOGGER = LoggerFactory.getLogger(AppConfig.class);

  @Value("${udp.host:#{null}}")
  private String udpHost;

  @Value("${udp.port}")
  private int udpPort;

  @Value("${udp.bufflen}")
  private int udpBuffLen;

  @Bean
  public UDPServer udpServerRetriever() throws UnknownHostException, SocketException {
    return new UDPServer(udpHost, udpPort, udpBuffLen);
  }
}
