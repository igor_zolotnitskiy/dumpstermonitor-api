package dumpster.monitor.api.events;

import dumpster.monitor.api.model.Dumpster;
import org.springframework.context.ApplicationEvent;

public final class DeleteDumpsterEvent extends ApplicationEvent {
    private static final long serialVersionUID = -540717475114734729L;
    private transient final int partNumber;

    public DeleteDumpsterEvent(final int partNumber) {
        super(partNumber);
        this.partNumber = partNumber;
    }

    public int getPartNumber() {
        return partNumber;
    }

    @Override
    public String toString() {
        return String.format("DeleteDumpsterEvent[partNumber=%d]", partNumber);
    }
}
