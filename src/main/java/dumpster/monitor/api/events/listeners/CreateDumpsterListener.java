package dumpster.monitor.api.events.listeners;

import dumpster.monitor.api.events.CreateDumpsterEvent;
import dumpster.monitor.api.ws.resources.WsController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public final class CreateDumpsterListener implements ApplicationListener<CreateDumpsterEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(CreateDumpsterListener.class);
    private final WsController wsController;

    @Inject
    public CreateDumpsterListener(final WsController wsController) {
        this.wsController = wsController;
    }

    @Override
    public void onApplicationEvent(CreateDumpsterEvent event) {
        LOG.info("Событие CreateDumpster {}", event);
        try {
            wsController.createDumpster(event.getDumpster());
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }
}
