package dumpster.monitor.api.events.listeners;

import dumpster.monitor.api.events.DeleteDumpsterEvent;
import dumpster.monitor.api.ws.resources.WsController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public final class DeleteDumpsterListener implements ApplicationListener<DeleteDumpsterEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(DeleteDumpsterListener.class);
    private final WsController wsController;

    @Inject
    public DeleteDumpsterListener(final WsController wsController) {
        this.wsController = wsController;
    }

    @Override
    public void onApplicationEvent(DeleteDumpsterEvent event) {
        LOG.info("Событие DeleteDumpster {}", event);
        try {
            wsController.deleteDumpster(event.getPartNumber());
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }
}
