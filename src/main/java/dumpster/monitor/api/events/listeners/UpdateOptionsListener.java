package dumpster.monitor.api.events.listeners;

import dumpster.monitor.api.events.UpdateOptionsEvent;
import dumpster.monitor.api.ws.resources.WsController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public final class UpdateOptionsListener implements ApplicationListener<UpdateOptionsEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(UpdateOptionsListener.class);
    private final WsController wsController;

    @Inject
    public UpdateOptionsListener(final WsController wsController) {
        this.wsController = wsController;
    }

    @Override
    public void onApplicationEvent(UpdateOptionsEvent event) {
        LOG.info("Событие UpdateOptions {}", event);
        try {
            wsController.updateOptions(event.getOptions());
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }
}
