package dumpster.monitor.api.repository.jpa;

import dumpster.monitor.api.model.Options;
import org.springframework.stereotype.Repository;

/**
 * @author Игорь Золотницкий
 */
@Repository
public interface OptionsRepository extends Java8Repository<Options, String> {
    Options findById(final String id);
}
