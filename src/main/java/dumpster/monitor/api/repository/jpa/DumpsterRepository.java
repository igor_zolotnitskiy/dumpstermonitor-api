package dumpster.monitor.api.repository.jpa;

import dumpster.monitor.api.model.Dumpster;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author Игорь Золотницкий
 */
@Repository
public interface DumpsterRepository extends Java8Repository<Dumpster, String> {
    Optional<Dumpster> findByPartNumber(final int partNumber);

    default boolean existsByPartNumber(final int partNumber) {
        return findByPartNumber(partNumber).isPresent();
    }

    @Modifying
    @Transactional
    @Query("DELETE FROM Dumpster dumpster where dumpster.partNumber = ?1")
    void deleteByPartNumber(int partNumber);
}
